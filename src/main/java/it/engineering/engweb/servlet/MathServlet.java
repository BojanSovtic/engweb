package it.engineering.engweb.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MathServlet
 */
@WebServlet("/math")
public class MathServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String operation = request.getParameter("operacija");
		try {
			Integer result1 = Integer.valueOf(request.getParameter("number1"));
			Integer result2 = Integer.valueOf(request.getParameter("number2"));

			switch (operation) {
			case "saberi":
				request.setAttribute("RESULT", result1 + result2);
				break;
			case "oduzmi":
				request.setAttribute("RESULT", result1 - result2);
				break;
			// druge opcije
			default:
				request.setAttribute("RESULT", "Nije podrzano");
			}
		} catch (Exception e) {
			request.setAttribute("RESULT", "Pogresno unete vrednosti!");
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/result.jsp");
		dispatcher.forward(request, response);
	}

}
